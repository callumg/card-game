package edu.ntnu.idatt2001.callumg.client;

import edu.ntnu.idatt2001.callumg.cards.DeckOfCards;
import edu.ntnu.idatt2001.callumg.cards.HandOfCards;
import edu.ntnu.idatt2001.callumg.cards.PlayingCard;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CardGameController implements Initializable {
    private DeckOfCards deckOfCards = new DeckOfCards();
    private ArrayList<PlayingCard> currentDeck = deckOfCards.dealHand(5);

    @FXML
    private ImageView card1 = new ImageView();

    @FXML
    private ImageView card2 = new ImageView();

    @FXML
    private ImageView card3 = new ImageView();

    @FXML
    private ImageView card4 = new ImageView();

    @FXML
    private ImageView card5 = new ImageView();

    @FXML
    private Text flush;

    @FXML
    private Text heartcards;

    @FXML
    private Text queenofspades;

    @FXML
    private Text sumofcards;

    @FXML
    void dealHand() {
        DeckOfCards deckOfCards = new DeckOfCards();
        try {
            currentDeck = deckOfCards.dealHand(5);
            showCards();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void checkHand() {
        HandOfCards handOfCards = new HandOfCards(currentDeck);
        StringBuilder sumOfCardsString = new StringBuilder("Sum: ");
        sumOfCardsString.append(handOfCards.getSumOfCards());
        sumofcards.setText(sumOfCardsString.toString());

        String hearts = "No heart cards";
        ArrayList<PlayingCard> heartsCards = handOfCards.getHeartCards();
        if (heartsCards.size() > 0) {
            StringBuilder heartsSB = new StringBuilder("Heart cards:");
            heartsCards.forEach((card) -> {
                heartsSB.append(" ").append(card.getAsString());
            });
            hearts = heartsSB.toString();
        }
        heartcards.setText(hearts);

        queenofspades.setText("Deck has queen of spades: " + handOfCards.containsQueenOfSpades());

        flush.setText("Deck is flush: " + handOfCards.handIsFlush());
    }

    private void showCards() {
        card1.setImage(new Image(currentDeck.get(0).getAsPng()));
        card2.setImage(new Image(currentDeck.get(1).getAsPng()));
        card3.setImage(new Image(currentDeck.get(2).getAsPng()));
        card4.setImage(new Image(currentDeck.get(3).getAsPng()));
        card5.setImage(new Image(currentDeck.get(4).getAsPng()));
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        showCards();
    }
}