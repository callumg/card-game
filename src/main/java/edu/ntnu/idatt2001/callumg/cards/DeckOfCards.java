package edu.ntnu.idatt2001.callumg.cards;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The type Deck of cards.
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private List<PlayingCard> cards;

    /**
     * Instantiates a new Deck of cards.
     */
    public DeckOfCards() {
        cards = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j <= 13; j++) {
                cards.add(new PlayingCard(suit[i], j));
            }
        }
    }

    /**
     * Gets deck of cards.
     *
     * @return the deck
     */
    public List<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Deal hand array list.
     *
     * @param n the number of cards to give.
     * @return the arraylist of cards to have on hand.
     * @throws IllegalArgumentException the illegal argument exception
     */
    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException {
        if (n <= 0) throw new IllegalArgumentException("You cannot deal 0 or less cards.");
        if (n > cards.size()) throw new IllegalArgumentException("You cannot deal more cards than are in the deck.");
        int totalCards;
        ArrayList<PlayingCard> cardsCopy = new ArrayList<>();
        ArrayList<PlayingCard> handOfCards = new ArrayList<>();
        cards.forEach(c -> cardsCopy.add(c));
        PlayingCard randomCard;
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            totalCards = cardsCopy.size();
            randomCard = cardsCopy.get(r.nextInt(totalCards));
            handOfCards.add(randomCard);
            cardsCopy.remove(randomCard);
        }
        return handOfCards;
    }
}
