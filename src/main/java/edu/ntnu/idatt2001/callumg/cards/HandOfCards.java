package edu.ntnu.idatt2001.callumg.cards;

import java.util.ArrayList;

/**
 * The type Hand of cards.
 *
 * @author Callum Gran
 * @version 0.1
 */
public class HandOfCards {
    private final ArrayList<PlayingCard> handOfCards;

    /**
     * Instantiates a new Hand of cards.
     *
     * @param handOfCards the hand of cards
     */
    public HandOfCards(ArrayList<PlayingCard> handOfCards) {
        this.handOfCards = handOfCards;
    }

    /**
     * Gets all heart cards in a hand.
     *
     * @return the heart cards
     */
    public ArrayList<PlayingCard> getHeartCards() {
        ArrayList<PlayingCard> heartsCards = new ArrayList<>();
        handOfCards.forEach((card) -> {
            if (card.getSuit() == 'H') heartsCards.add(card);
        });
        return heartsCards;
    }

    /**
     * Sums the cards in a hand.
     *
     * @return the sum of the cards
     */
    public int getSumOfCards() {
        return handOfCards.stream().mapToInt((card) -> card.getFace()).sum();
    }

    /**
     * Checks if the hand contains the queen of spades.
     *
     * @return true or false based on the statement
     */
    public boolean containsQueenOfSpades() {
        return handOfCards.stream().anyMatch((card) -> card.getAsString().equals("S12"));
    }

    /**
     * Checks if the hand is a flush.
     *
     * @return true or false based on the statement
     */
    public boolean handIsFlush() {
        char firstCardSuit = handOfCards.get(0).getSuit();
        return handOfCards.stream().allMatch((card) -> card.getSuit() == firstCardSuit);
    }
}
