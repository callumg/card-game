module edu.ntnu.idatt2001.callumg {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;

    opens edu.ntnu.idatt2001.callumg.client to javafx.fxml;
    exports edu.ntnu.idatt2001.callumg.client;
}
