package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.cards.HandOfCards;
import edu.ntnu.idatt2001.callumg.cards.PlayingCard;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {
    @Nested
    class getHeartCards {
        @Test
        void testGetHeartCardsReturnsEmptyListWhenNoHeartCardsArePresent() {
            ArrayList<PlayingCard> cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 8));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertEquals(new ArrayList<>(), handOfCards.getHeartCards());
        }
        @Test
        void testGetHeartCardsReturnsCorrectAmount() {
            ArrayList cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 8));
            cards.add(new PlayingCard('H', 4));
            cards.add(new PlayingCard('H', 5));
            cards.add(new PlayingCard('H', 6));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertEquals(3, handOfCards.getHeartCards().size());
        }
    }

    @Nested
    class getSumOfCards {
        @Test
        void testGetSumOfCardsReturnsCorrectValue() {
            ArrayList cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 8));
            cards.add(new PlayingCard('H', 4));
            cards.add(new PlayingCard('H', 5));
            cards.add(new PlayingCard('H', 6));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertEquals(23, handOfCards.getSumOfCards());
        }
        @Test
        void testGetSumOfCardsWhenNoCardsOnHand() {
            HandOfCards handOfCards = new HandOfCards(new ArrayList<>());
            assertEquals(0, handOfCards.getSumOfCards());
        }
    }

    @Nested
    class deckContainsQueenOfSpades {
        @Test
        void testContainsQueenOfSpadesReturnsTrueWhenHandHasQueenOfSpades() {
            ArrayList cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 8));
            cards.add(new PlayingCard('H', 4));
            cards.add(new PlayingCard('H', 5));
            cards.add(new PlayingCard('H', 6));
            cards.add(new PlayingCard('S', 12));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertTrue(handOfCards.containsQueenOfSpades());
        }
        @Test
        void testContainsQueenOfSpadesReturnsFalseWhenHandHasNotGotQueenOfSpades() {
            ArrayList cards = new ArrayList<>();
            cards.add(new PlayingCard('S', 8));
            cards.add(new PlayingCard('H', 4));
            cards.add(new PlayingCard('H', 5));
            cards.add(new PlayingCard('H', 6));
            cards.add(new PlayingCard('S', 13));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertFalse(handOfCards.containsQueenOfSpades());
        }
    }

    @Nested
    class handIsFlush {
        @Test
        void testHandIsFlushReturnsTrueWhenHandIsFlush() {
            ArrayList cards = new ArrayList<>();
            cards.add(new PlayingCard('H', 8));
            cards.add(new PlayingCard('H', 4));
            cards.add(new PlayingCard('H', 5));
            cards.add(new PlayingCard('H', 6));
            cards.add(new PlayingCard('H', 13));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertTrue(handOfCards.handIsFlush());
        }
        @Test
        void testHandIsFlushReturnsFalseWhenHandIsNotFlush() {
            ArrayList cards = new ArrayList<>();
            cards.add(new PlayingCard('H', 8));
            cards.add(new PlayingCard('H', 4));
            cards.add(new PlayingCard('H', 5));
            cards.add(new PlayingCard('H', 6));
            cards.add(new PlayingCard('S', 13));
            HandOfCards handOfCards = new HandOfCards(cards);
            assertFalse(handOfCards.handIsFlush());
        }
    }
}
