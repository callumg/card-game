package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.cards.DeckOfCards;
import edu.ntnu.idatt2001.callumg.cards.PlayingCard;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    @Nested
    class testDealHandExceptions {
        @Test
        void testCannotDealEmptyOrNegativeHand() {
            DeckOfCards deckOfCards = new DeckOfCards();
            try {
                deckOfCards.dealHand(0);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("You cannot deal 0 or less cards.", e.getMessage());
            }
        }
        @Test
        void testCannotDealMoreCardsThanInDeck() {
            DeckOfCards deckOfCards = new DeckOfCards();
            try {
                deckOfCards.dealHand(53);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("You cannot deal more cards than are in the deck.", e.getMessage());
            }
        }
    }

    @Nested
    class testDealHand {
        @Test
        void testDealtHandDoesNotContainDuplicates() {
            DeckOfCards deckOfCards = new DeckOfCards();
            ArrayList<PlayingCard> dealtHand = null;
            try {
                dealtHand = deckOfCards.dealHand(52);
            } catch (IllegalArgumentException e) {
                fail();
            }
            assertTrue(dealtHand.containsAll(deckOfCards.getCards()) && deckOfCards.getCards().containsAll(dealtHand));
        }
        @Test
        void testDealtHandIsCorrectSize() {
            DeckOfCards deckOfCards = new DeckOfCards();
            ArrayList<PlayingCard> dealtHand1 = null;
            ArrayList<PlayingCard> dealtHand2 = null;
            ArrayList<PlayingCard> dealtHand3 = null;
            try {
                dealtHand1 = deckOfCards.dealHand(10);
            } catch (IllegalArgumentException e) {
                fail();
            }
            try {
                dealtHand2 = deckOfCards.dealHand(12);
            } catch (IllegalArgumentException e) {
                fail();
            }
            try {
                dealtHand3 = deckOfCards.dealHand(1);
            } catch (IllegalArgumentException e) {
                fail();
            }
            assertEquals(10, dealtHand1.size());
            assertEquals(12, dealtHand2.size());
            assertEquals(1, dealtHand3.size());
        }
    }
}
